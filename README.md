# Quadruped

Dépendances à installer:

    pip install numpy scipy pybullet jupyter matplotlib

Ensuite:

    python quadruped.py -m mode [-x x-cible -y y-cible -t t-cible]

                        ---------------- Le mode leg_ik ----------------

          Cas non traîtés : Le cas x = y = 0 n'a pas été traité. on limite le curseur de x pour
          ne pas avoir des colisions. Le mouvement de la patte est fait d'une manière interpolée,
          ainsi la patte n'effectue pas de mouvement brusque. Dans ce mode, on renvoie la patta
          à sa dernière position atteignable si la position demandée par l'utilisateur ne l'est pas


                        ---------------- Le mode robot_ik ---------------

          Le mouvement du centre du robot dans son propre repère se fait de manière progressive
          (pas de mouvement brusque du robot). On limite les curseurs pour ne pas avoir des cas
          de singularité.


                        ---------------- Le mode walk ---------------  

          J'ai seulement implémenté la rotation du robot à une certaine vitesse angulaire. Le
          robot effectue un quart de tour à chaque cycle de rotation. L'idée est de modifier la
          durée de chaque cycle pour pouvoir respecter la bonne vitesse de rotation  



Nombre de solution angulaire pour atteindre une position x,y,z par une patte est 0,1,2 ou infini :
Ceci s'explique par le fait que les variables angulaires theta1, theta2 et theta3 décrivent un
système d'équations géométriques, en fait la solution de ce système correspond à l'intersection
d'une sphère, et deux plans d'où le nombre de solutions possible.
