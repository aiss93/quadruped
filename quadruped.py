import argparse
import math
import pybullet as p
from time import sleep
import matplotlib.pyplot as pyplot

dt = 0.01
L1 =0.04
L2 =0.045
L3 =0.065
L4 =0.087
omega = 0.0
in_cycle = False
t_ini = 0.0
t_fin = 0.0
x_i, y_i, z_i = L2+L3, 0.0, -L4
x_f, y_f, z_f = 0.0 ,0.0 ,0.0
x_att,y_att,z_att = 0.0 ,0.0 ,0.0


def init():
    """Initialise le simulateur

    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

def setJoints(robot, joints):
    """Définis les angles cibles pour les moteurs du robot

    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])


""" ***************          DEMONSTRATION           ******************** """



def demo(t, amplitude):
    """Démonstration de mouvement (fait osciller une patte)

    Arguments:
        t {float} -- Temps écoulé depuis le début de la simulation

    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
        float -- amplitude de l'oscillation
    """
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints


""" ***************          Fonctions d'interpolation           ******************** """


def interpolate(X,Y,x):
    """fonction d'interpolation

    Arguments:
        X {list} -- tableau des abscisses
        Y {list} -- tableau des ordonnées
        x {float} -- une abscisse

    Returns:
        flaot -- la valeur de la fonction interpolée en x
    """
    if(x<=X[0]) :
        return Y[0]
    if(x>=X[len(X)-1]) :
        return Y[len(X)-1]
    k = 0
    while(k<len(Y) and X[k] < x ):
        k = k + 1
    return (x-X[k-1])*(Y[k]-Y[k-1])/(X[k]-X[k-1])+Y[k-1]




def interpolate_joints(J1,J2,T,t):
    """fonction d'interpolation pour la liste des joints

    Arguments:
        T {list} -- tableau des abscisses (instants t)
        J1 {list} -- Les valeurs angulaires du robot à l'instat initiale
        J2 {list} -- Les valeurs angulaires du robot à l'instat finale
        t {float} -- l'instat auquel on aimerai connaitre les valeurs angulaires du tableau

    Returns:
        list -- Les valeurs angulaires du robots à l'instat t
    """
    joints = []
    for i in range (len(J1)) :
        joints.append(interpolate(T,[J1[i],J2[i]],t))
    return joints

""" ***************          La fonction Steady         ******************** """



#Cette fonction met le robot en position stable.
def steady(t) :
    """fonction de mise du robot en position stable

    Arguments:
        t {float} -- l'instat t

    Returns:
        list -- Les valeurs angulaires du robots à l'instat t pendant la mise en position stable
    """
    J1 = [0]*12
    joints = [0]*12
    joints[2] = math.pi/2
    joints[5] = math.pi/2
    joints[8] = math.pi/2
    joints[11] = math.pi/2
    return interpolate_joints(J1,joints,[0,0.4],t)




""" ***************          LEG_IK          ******************** """


def leg_ik_possible(X,Y,Z):
    """Positionner le bout de la patte 4 aux cordonnées X,Y et Z

    Arguments:
        X,Y,Z {float} -- le point où on veut placer la patte 4

    Returns:
        list -- Les valeurs angulaires du qu'il faut donner à la patte pour se positionner à X,Y,Z
    """
    T0 = math.atan2(Y,X)
    A = [L2*math.cos(T0),L2*math.sin(T0),0]
    AM = ((X-A[0])**2 + (Y-A[1])**2 + Z**2)**0.5
    T1= math.acos((AM**2 + L3**2 - L4**2)/(2*AM*L3)) + math.asin(Z/AM)
    T2= math.pi - math.acos((L3**2 + L4**2 - AM**2)/(2*L4*L3))
    return [T0,T1,T2]

def leg_ik(new_x,new_y,new_z):
    """Positionner le bout de la patte 4 aux cordonnées X,Y et Z
        quand ce point est atteignable sinon elle renvoit la patte au dernier point
        atteignable

    Arguments:
        new_x,new_y,new_z {float} -- le point où on veut placer la patte 4

    Returns:
        {list} -- Les valeurs angulaires du qu'il faut donner à la patte pour se positionner à X,Y,Z
    """
    global x_att,y_att,z_att
    T0 = math.atan2(new_y,new_x)
    A = [L2*math.cos(T0),L2*math.sin(T0),0]
    AM = ((new_x-A[0])**2 + (new_y-A[1])**2 + new_z**2)**0.5
    if L4-L3 <= AM <= L4+L3 :
        x_att,y_att,z_att = new_x,new_y,new_z
        return leg_ik_possible(new_x,new_y,new_z)
    else : #Si le point n'est pas atteignable on renvoit les le dernier point atteignable
        return leg_ik_possible(x_att,y_att,z_att)


def leg_ik_interpolated(new_x,new_y,new_z,t,joints) :
    """Positionner le bout de la patte 4 aux cordonnées X,Y et Z d'un manière progressive

    Arguments:
        new_x,new_y,new_z {float} -- le point où on veut placer la patte 4
        t {float} -- l'instant de la simulation
        joints {list} -- la liste des valeurs angulaire du robot

    Returns:
    """
    global in_cycle, x_i, y_i, z_i, x_f, y_f, z_f, t_ini, t_fin
    if in_cycle == False :
        t_ini = t
        t_fin = t + 0.2
        x_f, y_f, z_f = new_x,new_y,new_z
        in_cycle = True
    else :
        if t > t_fin :
            in_cycle = False
            x_i, y_i, z_i = x_f, y_f, z_f
            return
        X_t = interpolate([t_ini,t_fin],[x_i,x_f],t)
        Y_t = interpolate([t_ini,t_fin],[y_i,y_f],t)
        Z_t = interpolate([t_ini,t_fin],[z_i,z_f],t)
        joints[9],joints[10],joints[11] = leg_ik(X_t,Y_t,Z_t)
    return

""" ***************          ROBOT_IK          ******************** """

def get_default_pos(legID):
    """la position du bout la patte n° legID dans son propre repère

    Arguments:
        legID {integer} -- l'identifiant de la patte

    Returns:
        3 x {float} représentant la position du bout la patte n° legID dans son propre repère
    """
    x = (L2 + L3)
    y = 0
    z = - L4
    return x,y,z


def get_angle(legID) :
    """La position relative du repère de la patte n° legID par rapport au repère du robot

    Arguments:
        legID {integer} -- l'identifiant de la patte

    Returns:
        {float} La position relative du repère de la patte n° legID par rapport au repère du robot
    """
    if legID == 1 :
        return 3 * math.pi/4
    elif legID == 2 :
        return -3 * math.pi/4
    elif legID == 3 :
        return - math.pi/4
    elif legID == 4 :
        return math.pi/4

""" Fonction de changemen de repère """
def rotation_x(theta, x, y) :
    return x * math.cos(theta) + y * math.sin(theta)
def rotation_y(theta, x,y) :
    return y*math.cos(theta) - x*math.sin(theta)


def ComputeIKLeg(legID, x, y, z, extra_theta=0) :
    """les valeur angulaire de la patte n° legID pour mettre le robot à x,y,z

    Arguments:
        legID {integer} -- l'identifiant de la patte
        X,Y,Z {float} -- le point où on veut placer le centre du robot
        extra_theta {float} -- rotation supplémentaire

    Returns:
        {list} -- Les valeurs angulaires du qu'il faut donner à la patte legID pour que le robot se positionne à X,Y,Z
    """
    default_x, default_y, default_z = get_default_pos(legID)
    theta = get_angle(legID)+extra_theta
    new_x = default_x - rotation_x(theta, x, y)
    new_y = default_y - rotation_y(theta, x,y)
    new_z = default_z - z
    return leg_ik(new_x, new_y, new_z)


def robot_ik(X,Y,Z):
    """les valeur angulaire du robot pour se mettre à x,y,z

    Arguments:
        X,Y,Z {float} -- le point où on veut placer le centre du robot

    Returns:
        {list} -- Les valeurs angulaires du robto pour qu'il se positionne à X,Y,Z
    """
    return ComputeIKLeg(1,X,Y,Z, extra_theta=0)+ComputeIKLeg(2,X,Y,Z, extra_theta=0)+ComputeIKLeg(3,X,Y,Z, extra_theta=0)+ComputeIKLeg(4,X,Y,Z, extra_theta=0)


def robot_ik_interpolated(new_x,new_y,new_z,t,joints) :
    """les valeur angulaire du robot pour se mettre à x,y,z d'une manière progressive

    Arguments:
        new_x,new_y,new_z {float} -- le point où on veut placer le centre du robot
        joints {list} -- la liste des valeurs angulaires du robot
        t {float} -- l'instant t dans la simulation
    Returns:
    """
    global in_cycle, x_i, y_i, z_i, x_f, y_f, z_f, t_ini, t_fin
    L = []
    if in_cycle == False :
        t_ini = t
        t_fin = t + 0.2
        x_f, y_f, z_f = new_x,new_y,new_z
        in_cycle = True
    else :
        if t > t_fin :
            in_cycle = False
            x_i, y_i, z_i = x_f, y_f, z_f
            return
        X_t = interpolate([t_ini,t_fin],[x_i,x_f],t)
        Y_t = interpolate([t_ini,t_fin],[y_i,y_f],t)
        Z_t = interpolate([t_ini,t_fin],[z_i,z_f],t)
        L = robot_ik(X_t,Y_t,Z_t)
        for i in range(len(L)):
            joints[i] = L[i]
        return


""" ***************          Tourner le robot avec une vitesse angulaire          ******************** """


def z(Z,t_i,t_f,t) :
    """Fonction polynomiale en cloche qui modélise la montée et la descente de la patte

    Arguments:
        Z {float} -- La valeur minimale de la fonction
        t_i,t_f {list} -- l'intervalle de définition de cette fonction, en dehore de cet intervale z(t) et constante et vaut Z
        t {float} -- l'instant t dans la simulation
    Returns:
        {float} -- la valeur de z à l'instant t
    """
    if t >= t_f :
        return Z
    Z_m = - 0.044
    t_m = (t_f - t_i)/2
    return (Z - Z_m) * ((t-t_i-t_m)/t_m)**2 + Z_m

def rotate_leg(t_i,t_f,t,signe):
    """Cette fonction fait tourner une patte dans la direction (signe)

    Arguments:
        signe {float} -- le sens de la rotation
        t_i,t_f {list} -- l'intervalle de mouvement, en dehore de cet intervale la patte ne bouge pas
        t {float} -- l'instant t dans la simulation
    Returns:
        les valeur angulaire de la patte à l'instant t
    """
    default_x, default_y, default_z = get_default_pos(1)
    theta = interpolate([t_i,t_f],[0,signe * (math.pi/4 + math.asin(L1 * math.sin(math.pi/4)/(L2+L3)))],t)
    new_x = default_x * math.cos(theta) - default_y * math.sin(theta)
    new_y = default_y * math.cos(theta) + default_x * math.sin(theta)
    new_z = z(default_z,t_i,t_f,t)
    return leg_ik_possible(new_x,new_y,new_z)


def rotate_robot(w,joints,t):
    """Cette fonction fait tourner le robot à une vitesse angulaire w

    Arguments:
        w {float} -- la vitesse de rotation du robot
        joints {list} -- la liste des valeurs angulaires du robot
        t {float} -- l'instant t dans la simulation
    Returns: 
    """
    global in_cycle,t_ini,t_fin,omega
    if(in_cycle == False) :
        omega = w
        if omega == 0 :
            return
        t_ini = t
        t_fin = t_ini + (math.pi/4)/abs(omega)
        in_cycle = True
    else :
        if t >= t_fin + 0.2 :
            in_cycle = False
        elif t_fin < t <= t_fin + 0.2 :
            T = interpolate([t_fin,t_fin + 0.2],[(omega/abs(omega))*(math.pi/4 + math.asin(L1 * math.sin(math.pi/4)/(L2+L3))),0],t)
            joints[0],joints[3],joints[6],joints[9] = T, T,T,T
        elif t_ini <= t <= (t_ini + t_fin)/2 :
            T1, T2, T3 = rotate_leg(t_ini,(t_ini + t_fin)/2,t,omega/abs(omega))
            joints[0],joints[1],joints[2],joints[6],joints[7],joints[8] = T1, T2, T3,T1, T2, T3
        elif (t_ini + t_fin)/2 <= t <= t_fin :
            T1, T2, T3 = rotate_leg((t_ini + t_fin)/2,t_fin,t,omega/abs(omega))
            joints[3],joints[4],joints[5],joints[9],joints[10],joints[11] = T1, T2, T3,T1, T2, T3
        return



""" ********************************************************************************** """



if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, t = args.m, args.x, args.y, args.t


    if mode not in ['demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)
    robot = init()
    if mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')
    elif mode == 'leg_ik':
        x = p.addUserDebugParameter("x", -(L2+L3+L4), L2+L3+L4, L2+L3)
        y = p.addUserDebugParameter("y", -(L2+L3+L4), L2+L3+L4, 0)
        z = p.addUserDebugParameter("z", -L4, L2+L3+L4, -L4)
        print('Mode de leg_ik...')
    elif mode == 'robot_ik':
        x = p.addUserDebugParameter("x", -math.sqrt((L1**2)/2), math.sqrt((L1**2)/2), 0)
        y = p.addUserDebugParameter("y", -math.sqrt((L1**2)/2), math.sqrt((L1**2)/2), 0)
        z = p.addUserDebugParameter("z", -L4, L2+L3, 0)
        x_i, y_i, z_i = 0,0,0
        print('Mode de robot_ik...')
    elif mode == 'walk':
        theta = p.addUserDebugParameter("Vitesse angulaire", -math.pi/2, math.pi/2, math.pi/4)
    else:
        raise Exception('Mode non implémenté: %s' % mode)

    t = 0

    # Boucle principale
    while True:
        t += dt

        if mode == 'demo':
            # Récupération des positions cibles
            joints = demo(t, p.readUserDebugParameter(amplitude))

        elif mode == 'leg_ik':
            if 0 <= t < 0.5 : #On stabilise d'abord le robot
                joints = steady(t)
            else :
                X = p.readUserDebugParameter(x)
                Y = p.readUserDebugParameter(y)
                Z = p.readUserDebugParameter(z)
                leg_ik_interpolated(X,Y,Z,t,joints)


        elif mode == 'robot_ik':
            if 0<= t < 0.5 :
                joints = steady(t)
            else :
                X = p.readUserDebugParameter(x)
                Y = p.readUserDebugParameter(y)
                Z = p.readUserDebugParameter(z)
                robot_ik_interpolated(X,Y,Z,t,joints)

        elif mode == 'walk':
            if 0<= t < 0.5 :
                joints = steady(t)
            else :
                w = p.readUserDebugParameter(theta)
                rotate_robot(w,joints,t)



        # Envoi des positions cibles au simulateur
        setJoints(robot, joints)

        # Mise à jour de la simulation
        p.stepSimulation()
        sleep(dt)
